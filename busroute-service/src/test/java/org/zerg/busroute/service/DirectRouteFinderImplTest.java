package org.zerg.busroute.service;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.zerg.busroute.service.impl.DirectRouteFinderImpl;
import org.zerg.busroute.service.impl.RoutesInMemoryRepository;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by zerg on 01.12.2016.
 */
public class DirectRouteFinderImplTest {

    private DirectRouteFinder directRouteFinder;

    @Before
    public void init() throws IOException {
        String routesPath = new ClassPathResource("test_routes_data.txt").getFile().getAbsolutePath();
        directRouteFinder = new DirectRouteFinderImpl(new RoutesInMemoryRepository(routesPath));
    }

    @Test
    public void shouldFindExistingDirectRoute() {
        assertTrue(directRouteFinder.isDirectRoute(0, 4));
    }

    @Test
    public void shouldNotFindNotExistingDirectRoute() {
        assertFalse(directRouteFinder.isDirectRoute(2, 5));
    }


}