package org.zerg.busroute.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.zerg.busroute.service.DirectRouteFinder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by zerg on 30.11.2016.
 */
public class RoutesInMemoryRepositoryTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldTrowIllegalArgumentExceptionWhenRoutePathIsAbsent() throws IOException {
        new RoutesInMemoryRepository("");
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldFileNotFoundExceptionWhenRoutePathIsWrong() throws IOException {
        new RoutesInMemoryRepository("tets.txt");
    }

    @Test
    public void shouldLoadBusRoutes() throws IOException {
        Map<Integer, List<Integer>> expectedRepository = new HashMap<>();
        expectedRepository.put(0, asList(0, 1, 2, 3, 4));
        expectedRepository.put(1, asList(3, 1, 6, 5));
        expectedRepository.put(2, asList(0, 6, 4));

        String routesPath = new ClassPathResource("test_routes_data.txt").getFile().getAbsolutePath();
        RoutesInMemoryRepository repository = new RoutesInMemoryRepository(routesPath);

        assertEquals(expectedRepository, repository.getBusRoutes());
    }

}