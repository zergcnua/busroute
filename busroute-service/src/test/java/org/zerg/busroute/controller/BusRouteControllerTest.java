package org.zerg.busroute.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.zerg.busroute.controller.dto.BusRouteResponse;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by zerg on 30.11.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BusRouteControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldFindExistingDirectRoute() throws Exception {
        int depSid = 3;
        int arrSid = 5;
        BusRouteResponse response = restTemplate
                .getForObject("/api/direct?dep_sid=" + depSid + "&arr_sid=" + arrSid, BusRouteResponse.class);

        assertEquals(depSid, response.getDepartureSid());
        assertEquals(arrSid, response.getArrivalSid());
        assertTrue(response.isDirectRoute());
    }

    @Test
    public void shouldNotFindNotExistingDirectRoute() throws Exception {
        int depSid = 3;
        int arrSid = 8;
        BusRouteResponse response = restTemplate
                .getForObject("/api/direct?dep_sid=" + depSid + "&arr_sid=" + arrSid, BusRouteResponse.class);

        assertEquals(depSid, response.getDepartureSid());
        assertEquals(arrSid, response.getArrivalSid());
        assertFalse(response.isDirectRoute());
    }

}