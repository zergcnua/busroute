package org.zerg.busroute.service;

import java.util.List;
import java.util.Map;

/**
 * Created by zerg on 27.11.2016.
 */
public interface RoutesRepository {

    Map<Integer, List<Integer>> getBusRoutes();

}
