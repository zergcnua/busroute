package org.zerg.busroute.service;

/**
 * Created by zerg on 01.12.2016.
 */
public interface DirectRouteFinder {

    boolean isDirectRoute(int depSid, int arrSid);

}
