package org.zerg.busroute.service.impl;

import org.zerg.busroute.service.RoutesRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableMap;
import static org.apache.commons.io.FileUtils.getFile;
import static org.apache.commons.io.FileUtils.readLines;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.Validate.isTrue;

/**
 * Created by zerg on 27.11.2016.
 */
public class RoutesInMemoryRepository implements RoutesRepository {

    private Map<Integer, List<Integer>> repository;

    public RoutesInMemoryRepository(String routesPath) throws IOException {
        isTrue(isNotBlank(routesPath), "routes import path can't be empty");
        repository = initRepository(routesPath);
    }

    private Map<Integer, List<Integer>> initRepository(String routesSourcePath) throws IOException {
        File routesSource = getFile(routesSourcePath);
        List<String> rawRoutesData = readLines(routesSource, UTF_8);
        return rawRoutesData.stream()
                .map(String::trim)
                .filter(line -> line.length() > 1)
                .collect(Collectors.toMap(getRouteNumber(), getRoutStops()));
    }

    private Function<String, Integer> getRouteNumber() {
        return line -> Integer.valueOf(line.split(" ")[0]);
    }

    private Function<String, List<Integer>> getRoutStops() {
        return line -> {
            List<String> routesStops = new ArrayList<>(asList(line.split(" ")));
            routesStops.remove(0);
            return routesStops.stream()
                    .map(Integer::valueOf)
                    .collect(Collectors.toList());
        };
    }

    @Override
    public Map<Integer, List<Integer>> getBusRoutes() {
        return unmodifiableMap(repository);
    }
}
