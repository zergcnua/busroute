package org.zerg.busroute.service.impl;

import org.zerg.busroute.service.DirectRouteFinder;
import org.zerg.busroute.service.RoutesRepository;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Created by zerg on 01.12.2016.
 */
public class DirectRouteFinderImpl implements DirectRouteFinder {

    private RoutesRepository repository;

    public DirectRouteFinderImpl(RoutesRepository repository) {
        notNull(repository, "repository must be initialized");
        this.repository = repository;
    }

    @Override
    public boolean isDirectRoute(int depSid, int arrSid) {
        return repository.getBusRoutes().values().stream()
                .filter(stops -> stops.contains(depSid) && stops.contains(arrSid))
                .findFirst()
                .isPresent();
    }

}
