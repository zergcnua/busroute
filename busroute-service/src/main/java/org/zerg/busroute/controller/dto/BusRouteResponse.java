package org.zerg.busroute.controller.dto;

/**
 * Created by zerg on 26.11.2016.
 */
public class BusRouteResponse {

    private int departureSid;
    private int arrivalSid;
    private boolean directRoute;

    public int getDepartureSid() {
        return departureSid;
    }

    public void setDepartureSid(int departureSid) {
        this.departureSid = departureSid;
    }

    public int getArrivalSid() {
        return arrivalSid;
    }

    public void setArrivalSid(int arrivalSid) {
        this.arrivalSid = arrivalSid;
    }

    public boolean isDirectRoute() {
        return directRoute;
    }

    public void setDirectRoute(boolean directRoute) {
        this.directRoute = directRoute;
    }
}
