package org.zerg.busroute.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zerg.busroute.service.DirectRouteFinder;
import org.zerg.busroute.service.RoutesRepository;
import org.zerg.busroute.controller.dto.BusRouteResponse;

/**
 * Created by zerg on 26.11.2016.
 */
@RestController
public class BusRouteController {

    @Autowired
    private DirectRouteFinder directRouteFinder;

    @RequestMapping(value = "/api/direct", method = RequestMethod.GET)
    @ResponseBody
    public BusRouteResponse route(@RequestParam("dep_sid") int depSid, @RequestParam("arr_sid") int arrSid) {
        BusRouteResponse busRouteResponse = new BusRouteResponse();

        busRouteResponse.setDepartureSid(depSid);
        busRouteResponse.setArrivalSid(arrSid);
        busRouteResponse.setDirectRoute(directRouteFinder.isDirectRoute(depSid, arrSid));

        return busRouteResponse;
    }

}
