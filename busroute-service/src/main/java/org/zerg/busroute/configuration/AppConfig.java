package org.zerg.busroute.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zerg.busroute.service.DirectRouteFinder;
import org.zerg.busroute.service.RoutesRepository;
import org.zerg.busroute.service.impl.DirectRouteFinderImpl;
import org.zerg.busroute.service.impl.RoutesInMemoryRepository;

import java.io.IOException;

/**
 * Created by zerg on 27.11.2016.
 */
@Configuration
@EnableConfigurationProperties(AppProperties.class)
public class AppConfig {

    @Autowired
    private AppProperties appProperties;

    @Bean
    public RoutesRepository routesRepository() throws IOException {
        return new RoutesInMemoryRepository(appProperties.getRoutesPath());
    }

    @Bean
    public DirectRouteFinder directRouteFinder() throws IOException {
        return new DirectRouteFinderImpl(routesRepository());
    }

}
