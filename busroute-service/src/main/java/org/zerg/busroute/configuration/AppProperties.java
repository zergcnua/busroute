package org.zerg.busroute.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Created by zerg on 01.12.2016.
 */
@ConfigurationProperties(prefix = "busroute")
public class AppProperties {

    private Resource routes;

    public Resource getRoutes() {
        return routes;
    }

    public void setRoutes(Resource routes) {
        this.routes = routes;
    }

    public String getRoutesPath() throws IOException {
        return routes.getFile().getAbsolutePath();
    }
}
