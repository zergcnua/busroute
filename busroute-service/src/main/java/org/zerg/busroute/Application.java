package org.zerg.busroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by zerg on 26.11.2016.
 */
@SpringBootApplication
public class Application {

    //in order to run add program argument e.g. --busroute.routes=file:d:/routes.txt
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}
